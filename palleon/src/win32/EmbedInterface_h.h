

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.01.0622 */
/* at Tue Jan 19 03:14:07 2038
 */
/* Compiler settings for ..\src\win32\EmbedInterface.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.01.0622 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */



/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 500
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif /* __RPCNDR_H_VERSION__ */

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __EmbedInterface_h_h__
#define __EmbedInterface_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IEmbedApplication_FWD_DEFINED__
#define __IEmbedApplication_FWD_DEFINED__
typedef interface IEmbedApplication IEmbedApplication;

#endif 	/* __IEmbedApplication_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IEmbedApplication_INTERFACE_DEFINED__
#define __IEmbedApplication_INTERFACE_DEFINED__

/* interface IEmbedApplication */
/* [uuid][object] */ 


EXTERN_C const IID IID_IEmbedApplication;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("602BE7B1-9DD8-4B20-A432-61BB6F44AA5C")
    IEmbedApplication : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Update( 
            /* [in] */ float dt) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetSurfaceHandle( 
            /* [out] */ DWORD_PTR *surfaceHandle) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE SetSurfaceSize( 
            /* [in] */ unsigned int width,
            /* [in] */ unsigned int height) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NotifyMouseMove( 
            /* [in] */ int posX,
            /* [in] */ int posY) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NotifyMouseWheel( 
            /* [in] */ int posZ) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NotifyMouseDown( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NotifyMouseUp( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NotifyKeyDown( 
            /* [in] */ unsigned int code) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NotifyKeyUp( 
            /* [in] */ unsigned int code) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE NotifyExternalCommand( 
            /* [in] */ BSTR command,
            /* [retval][out] */ BSTR *result) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetEvents( 
            /* [retval][out] */ SAFEARRAY * *result) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IEmbedApplicationVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEmbedApplication * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEmbedApplication * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEmbedApplication * This);
        
        HRESULT ( STDMETHODCALLTYPE *Update )( 
            IEmbedApplication * This,
            /* [in] */ float dt);
        
        HRESULT ( STDMETHODCALLTYPE *GetSurfaceHandle )( 
            IEmbedApplication * This,
            /* [out] */ DWORD_PTR *surfaceHandle);
        
        HRESULT ( STDMETHODCALLTYPE *SetSurfaceSize )( 
            IEmbedApplication * This,
            /* [in] */ unsigned int width,
            /* [in] */ unsigned int height);
        
        HRESULT ( STDMETHODCALLTYPE *NotifyMouseMove )( 
            IEmbedApplication * This,
            /* [in] */ int posX,
            /* [in] */ int posY);
        
        HRESULT ( STDMETHODCALLTYPE *NotifyMouseWheel )( 
            IEmbedApplication * This,
            /* [in] */ int posZ);
        
        HRESULT ( STDMETHODCALLTYPE *NotifyMouseDown )( 
            IEmbedApplication * This);
        
        HRESULT ( STDMETHODCALLTYPE *NotifyMouseUp )( 
            IEmbedApplication * This);
        
        HRESULT ( STDMETHODCALLTYPE *NotifyKeyDown )( 
            IEmbedApplication * This,
            /* [in] */ unsigned int code);
        
        HRESULT ( STDMETHODCALLTYPE *NotifyKeyUp )( 
            IEmbedApplication * This,
            /* [in] */ unsigned int code);
        
        HRESULT ( STDMETHODCALLTYPE *NotifyExternalCommand )( 
            IEmbedApplication * This,
            /* [in] */ BSTR command,
            /* [retval][out] */ BSTR *result);
        
        HRESULT ( STDMETHODCALLTYPE *GetEvents )( 
            IEmbedApplication * This,
            /* [retval][out] */ SAFEARRAY * *result);
        
        END_INTERFACE
    } IEmbedApplicationVtbl;

    interface IEmbedApplication
    {
        CONST_VTBL struct IEmbedApplicationVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEmbedApplication_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IEmbedApplication_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IEmbedApplication_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IEmbedApplication_Update(This,dt)	\
    ( (This)->lpVtbl -> Update(This,dt) ) 

#define IEmbedApplication_GetSurfaceHandle(This,surfaceHandle)	\
    ( (This)->lpVtbl -> GetSurfaceHandle(This,surfaceHandle) ) 

#define IEmbedApplication_SetSurfaceSize(This,width,height)	\
    ( (This)->lpVtbl -> SetSurfaceSize(This,width,height) ) 

#define IEmbedApplication_NotifyMouseMove(This,posX,posY)	\
    ( (This)->lpVtbl -> NotifyMouseMove(This,posX,posY) ) 

#define IEmbedApplication_NotifyMouseWheel(This,posZ)	\
    ( (This)->lpVtbl -> NotifyMouseWheel(This,posZ) ) 

#define IEmbedApplication_NotifyMouseDown(This)	\
    ( (This)->lpVtbl -> NotifyMouseDown(This) ) 

#define IEmbedApplication_NotifyMouseUp(This)	\
    ( (This)->lpVtbl -> NotifyMouseUp(This) ) 

#define IEmbedApplication_NotifyKeyDown(This,code)	\
    ( (This)->lpVtbl -> NotifyKeyDown(This,code) ) 

#define IEmbedApplication_NotifyKeyUp(This,code)	\
    ( (This)->lpVtbl -> NotifyKeyUp(This,code) ) 

#define IEmbedApplication_NotifyExternalCommand(This,command,result)	\
    ( (This)->lpVtbl -> NotifyExternalCommand(This,command,result) ) 

#define IEmbedApplication_GetEvents(This,result)	\
    ( (This)->lpVtbl -> GetEvents(This,result) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IEmbedApplication_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  LPSAFEARRAY_UserSize(     unsigned long *, unsigned long            , LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserMarshal(  unsigned long *, unsigned char *, LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserUnmarshal(unsigned long *, unsigned char *, LPSAFEARRAY * ); 
void                      __RPC_USER  LPSAFEARRAY_UserFree(     unsigned long *, LPSAFEARRAY * ); 

unsigned long             __RPC_USER  BSTR_UserSize64(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal64(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal64(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree64(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  LPSAFEARRAY_UserSize64(     unsigned long *, unsigned long            , LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserMarshal64(  unsigned long *, unsigned char *, LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserUnmarshal64(unsigned long *, unsigned char *, LPSAFEARRAY * ); 
void                      __RPC_USER  LPSAFEARRAY_UserFree64(     unsigned long *, LPSAFEARRAY * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


