#pragma once

#include <vector>
#include <map>
#include <unordered_map>
#include <memory>
#include "Stream.h"

class CMapLayout
{
public:
   struct Position
   {
      float x, y, z;
      float rotX, rotY, rotZ;
      float scaleX, scaleY, scaleZ;
   };
	struct RESOURCE_ITEM
	{
		char			name[0x10];
		char			type[4];
		uint32			resourceId;
		uint32			unknown1;
		uint32			unknown2;
	};
	static_assert(sizeof(RESOURCE_ITEM) == 0x20, "RESOURCE_ITEM size must be 32 bytes.");
	typedef std::vector<RESOURCE_ITEM> ResourceItemArray;

	struct LAYOUT_NODE
	{
		virtual			~LAYOUT_NODE() {}

		uint32			nodeId;
		uint32			parentPtr;
		std::string		name;
	};
	typedef std::shared_ptr<LAYOUT_NODE> LayoutNodePtr;
	typedef std::map<uint32, LayoutNodePtr> LayoutNodeMap;

   struct LAY_SETTINGS_OBJECT
   {
      uint32_t nodeId;
      uint32_t parentNodePtr;
      uint32_t namePtr;

      unsigned int unknown1[4];
      unsigned int namePtr2;
      unsigned int unknown2[8];
      float posX, posY, posZ;
   };

	struct UNIT_TREE_OBJECT_ITEM
	{
      std::string    resourceName;
		std::string		name;
		uint32			nodePtr;
	};
	typedef std::vector<UNIT_TREE_OBJECT_ITEM> UnitTreeObjectItemArray;

	struct UNIT_TREE_OBJECT_NODE : public LAYOUT_NODE
	{
		UnitTreeObjectItemArray		items;
	};

	struct INSTANCE_OBJECT_NODE : public LAYOUT_NODE
	{
		uint32			refNodePtr;
      std::string resourceName;
      std::string refNodeName;
		float			posX, posY, posZ;
		float			rotX, rotY, rotZ;
      float       scaleX, scaleY, scaleZ;
	};

	struct BGPARTS_BASE_OBJECT_NODE : public LAYOUT_NODE
	{
		std::string		modelName;
		std::string		resourceName;
		float			minX, minY, minZ;
		float			maxX, maxY, maxZ;
	};

								CMapLayout();
	virtual						~CMapLayout();

	void						Read(Framework::CStream&);

	const ResourceItemArray&	GetResourceItems() const;
	const LayoutNodeMap&		GetLayoutNodes() const;
   std::unordered_map<std::string, std::vector<std::string>> resourceGroup;
   std::unordered_map<std::string, std::vector<Position>> resourcePositions;
   LAY_SETTINGS_OBJECT layoutSettings;

//private:
	ResourceItemArray			m_resourceItems;
	LayoutNodeMap				m_layoutNodes;
};

typedef std::shared_ptr<CMapLayout> MapLayoutPtr;
