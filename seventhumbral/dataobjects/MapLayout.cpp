#include <vector>
#include <algorithm>
#include <map>
#include "MapLayout.h"
#include <fstream>
#include <regex>

//#define _TRACE_TREE

#ifdef _TRACE_TREE
#include <Windows.h>
#include "string_format.h"
#endif

struct UNK1
{
	char offset1[4];
	char offset2[4];
	char someFlags1[4];
	char someFlags2[4];
};
std::vector<std::string> filters;

CMapLayout::CMapLayout()
{
   std::ifstream infile( "./data/filter.filter" );
   std::string line = "";

   filters.clear();
   if( infile.good() )
   {
      while( std::getline( infile, line ) )
      {
         if( line[0] == '\0' || line[0] == '#' )
            continue;

         std::string pushLn = "";
         for( auto c : line )
         {
            if( c == '\r' || c == '\n' || c == '\0' || c == '#' )
               break;
            pushLn += c;
         }
         if( pushLn.size() >= 5 )
            filters.push_back( pushLn );
      }
   }
}

CMapLayout::~CMapLayout()
{

}

const CMapLayout::ResourceItemArray& CMapLayout::GetResourceItems() const
{
	return m_resourceItems;
}

const CMapLayout::LayoutNodeMap& CMapLayout::GetLayoutNodes() const
{
	return m_layoutNodes;
}

using namespace std; //Don't if you're in a header-file

template<typename ... Args>
string string_format( const std::string& format, Args ... args )
{
   size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
   unique_ptr<char[]> buf( new char[size] );
   snprintf( buf.get(), size, format.c_str(), args ... );
   return string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}

void CMapLayout::Read(Framework::CStream& inputStream)
{
	uint8 fileId[0x20];
	inputStream.Read(fileId, 0x20);
	uint32 headerSize = inputStream.Read32();
	uint32 resourceItemCount = inputStream.Read32();

	//Read resource items
	inputStream.Seek(0x40, Framework::STREAM_SEEK_SET);
	m_resourceItems.resize(resourceItemCount);
	for(auto& resourceItem : m_resourceItems)
	{
		inputStream.Read(&resourceItem, sizeof(RESOURCE_ITEM));
	}

	//Read scene tree
	inputStream.Seek(headerSize, Framework::STREAM_SEEK_SET);

	//Skip SEDB header
	inputStream.Seek(0x30, Framework::STREAM_SEEK_CUR);

	uint32 lybMagic = inputStream.Read32();
	uint32 lybSize = inputStream.Read32();
	uint16 unk1Count = inputStream.Read16();
	uint16 nodeCount = inputStream.Read16();

	//Skip some other headers
	inputStream.Seek(0x0C, Framework::STREAM_SEEK_CUR);

	std::vector<uint32> nodePtrs;
	nodePtrs.resize(nodeCount);
	inputStream.Read(nodePtrs.data(), nodeCount * sizeof(uint32));

	std::vector<UNK1> unk1s;
	unk1s.resize(unk1Count);
	inputStream.Read(unk1s.data(), unk1Count * sizeof(UNK1));

	std::map<uint32, std::string> nodeNames;

	for(unsigned int i = 0; i < nodeCount; i++)
	{
		uint32 nodePtr = nodePtrs[i];

		inputStream.Seek(nodePtr + headerSize + 0x30, Framework::STREAM_SEEK_SET);
		uint32 nodeHeader[3];
		inputStream.Read(nodeHeader, sizeof(nodeHeader));

		inputStream.Seek(nodeHeader[2] + headerSize + 0x30, Framework::STREAM_SEEK_SET);

		auto nodeName = inputStream.ReadString();
		nodeNames[nodePtr] = nodeName;
	}

   // credit for groups to showmo
   auto getResourceName = [&](uint32_t nodeOffset) -> std::string
   {
      uint64_t oldOffset = inputStream.Tell();

      // move to this offset
      uint32 nodeAbsPtr = nodeOffset + headerSize + 0x30;
      inputStream.Seek(nodeAbsPtr, Framework::STREAM_SEEK_SET);
      
      // read header
      uint32 nodeHeader[3];
      inputStream.Read(nodeHeader, sizeof(nodeHeader));
      inputStream.Seek(nodeHeader[2] + headerSize + 0x30, Framework::STREAM_SEEK_SET);
      // read the info into vars
      auto nodeName = inputStream.ReadString();
      uint32 nodeId = nodeHeader[0];
      uint32 parentPtr = nodeHeader[1];


      // seek to parent node and read it in
      inputStream.Seek(parentPtr + headerSize + 0x30, Framework::STREAM_SEEK_SET);
      inputStream.Read(nodeHeader, sizeof(nodeHeader));
      inputStream.Seek(nodeHeader[2] + headerSize + 0x30, Framework::STREAM_SEEK_SET);

      auto parentNodeName = inputStream.ReadString();

      if (parentNodeName == "BaseObjects/BG/BGPartsBaseObject" || parentNodeName == "BaseObjects/BG/BGChipBaseObject")
      {
         uint8 nodeData[0x54];

         inputStream.Seek(nodeAbsPtr, Framework::STREAM_SEEK_SET);
         inputStream.Read(nodeData, sizeof(nodeData));

         uint32 someStringPtr1 = *reinterpret_cast<uint32*>(nodeData + 0x08);
         uint32 someStringPtr2 = *reinterpret_cast<uint32*>(nodeData + 0x20);

         uint32 someStringAbsPtr1 = someStringPtr1 + headerSize + 0x30;
         uint32 someStringAbsPtr2 = someStringPtr2 + headerSize + 0x30;

         inputStream.Seek(someStringAbsPtr1, Framework::STREAM_SEEK_SET);
         auto modelName = inputStream.ReadString();

         inputStream.Seek(someStringAbsPtr2, Framework::STREAM_SEEK_SET);
         auto resourceName = inputStream.ReadString();


         float vec0x = *reinterpret_cast<float*>(nodeData + 0x3C);
         float vec0y = *reinterpret_cast<float*>(nodeData + 0x40);
         float vec0z = *reinterpret_cast<float*>(nodeData + 0x44);

         float vec1x = *reinterpret_cast<float*>(nodeData + 0x48);
         float vec1y = *reinterpret_cast<float*>(nodeData + 0x4C);
         float vec1z = *reinterpret_cast<float*>(nodeData + 0x50);

         inputStream.Seek(someStringAbsPtr1, Framework::STREAM_SEEK_SET);

         inputStream.Seek(someStringAbsPtr2, Framework::STREAM_SEEK_SET);
         auto bgPartsBaseObjectNode = std::make_shared<BGPARTS_BASE_OBJECT_NODE>();
         bgPartsBaseObjectNode->modelName = modelName;
         bgPartsBaseObjectNode->resourceName = resourceName;
         bgPartsBaseObjectNode->minX = vec0x;
         bgPartsBaseObjectNode->minY = vec0y;
         bgPartsBaseObjectNode->minZ = vec0z;
         bgPartsBaseObjectNode->maxX = vec1x;
         bgPartsBaseObjectNode->maxY = vec1y;
         bgPartsBaseObjectNode->maxZ = vec1z;
         bgPartsBaseObjectNode->nodeId = nodeId;

         m_layoutNodes.emplace(std::make_pair(nodeOffset, bgPartsBaseObjectNode));

         inputStream.Seek(oldOffset, Framework::STREAM_SEEK_SET);
         return resourceName;
      }
      else if (parentNodeName == "RefObjects/InstanceObjects")
      {
         std::string placeholder;
      }
      //*
      else if (parentNodeName == "BaseObjects/Attribute/AttributeBaseObject")
      {
         uint8 nodeData[0x54];

         inputStream.Seek(nodeAbsPtr, Framework::STREAM_SEEK_SET);
         inputStream.Read(nodeData, sizeof(nodeData));

         uint32 someStringPtr1 = *reinterpret_cast<uint32*>(nodeData + 0x08);
         uint32 someStringPtr2 = *reinterpret_cast<uint32*>(nodeData + 0x24);

         uint32 someStringAbsPtr1 = someStringPtr1 + headerSize + 0x30;
         uint32 someStringAbsPtr2 = someStringPtr2 + headerSize + 0x30;

         inputStream.Seek(someStringAbsPtr1, Framework::STREAM_SEEK_SET);
         auto modelName = inputStream.ReadString();

         inputStream.Seek(someStringAbsPtr2, Framework::STREAM_SEEK_SET);
         auto resourceName = inputStream.ReadString();

         inputStream.Seek(oldOffset, Framework::STREAM_SEEK_SET);
         return resourceName;
      }
      //*/
      inputStream.Seek(oldOffset, Framework::STREAM_SEEK_SET);
      return "";
   };

   std::map< std::string, int > parentNodeNames;
   int unitTreeItemCount = 0;

   for (unsigned int i = 1; i < nodeCount; i++)
   {
      uint32 nodePtr = nodePtrs[i];
      uint32 nodeAbsPtr = nodePtr + headerSize + 0x30;

      inputStream.Seek(nodeAbsPtr, Framework::STREAM_SEEK_SET);
      uint32 nodeHeader[3];
      inputStream.Read(nodeHeader, sizeof(nodeHeader));

      uint32 nodeId = nodeHeader[0];
      uint32 parentPtr = nodeHeader[1];

      auto nodeName = nodeNames[nodePtr];
      auto parentNodeName = nodeNames[nodeHeader[1]];

      parentNodeNames[parentNodeName]++;
      if (parentNodeName == "")
         continue;

#ifdef _TRACE_TREE
      OutputDebugStringA(string_format("Id: 0x%0.8X, Ptr: 0x%0.8X(0x%0.8X), Name: %s, Parent Name: %s\r\n",
         nodeHeader[0], nodePtr, nodeAbsPtr, nodeName.c_str(), parentNodeName.c_str()).c_str());
#endif
      auto f = fopen("log.txt", "ab+");

      LayoutNodePtr result = nullptr;
      std::string filterModelName = "";
      std::string type = "";


		if(parentNodeName == "RefObjects/InstanceObject")
		{
			auto instanceObjectNode = std::make_shared<INSTANCE_OBJECT_NODE>();

			uint32 nodeData[0x10];

			//0x08 -> Pos X?
			//0x09 -> Pos Y?
			//0x0A -> Pos Z?
			//0x0F -> Ref Node Ptr?

			inputStream.Seek(nodeAbsPtr, Framework::STREAM_SEEK_SET);
			inputStream.Read(nodeData, sizeof(nodeData));

			instanceObjectNode->posX = *reinterpret_cast<float*>(&nodeData[0x08]);
			instanceObjectNode->posY = *reinterpret_cast<float*>(&nodeData[0x09]);
			instanceObjectNode->posZ = *reinterpret_cast<float*>(&nodeData[0x0A]);

			instanceObjectNode->refNodePtr = nodeData[0x0F];
         inputStream.Seek(instanceObjectNode->refNodePtr, Framework::STREAM_SEEK_SET);
         std::string refNodeName = nodeNames[instanceObjectNode->refNodePtr];
         instanceObjectNode->name = nodeNames[nodePtr];

			uint32 rotAbsPtr = nodeData[0x0B] + headerSize + 0x30;
			uint32 scaleAbsPtr = nodeData[0x0C] + headerSize + 0x30;

			float rotData[3];
         float scaleData[3];

			inputStream.Seek(rotAbsPtr, Framework::STREAM_SEEK_SET);
			inputStream.Read(rotData, sizeof(rotData));
         inputStream.Seek(scaleAbsPtr, Framework::STREAM_SEEK_SET);
         inputStream.Read(scaleData, sizeof(scaleData));

			instanceObjectNode->rotX = rotData[0];
			instanceObjectNode->rotY = rotData[1];
			instanceObjectNode->rotZ = rotData[2];

         instanceObjectNode->scaleX = scaleData[0];
         instanceObjectNode->scaleY = scaleData[1];
         instanceObjectNode->scaleZ = scaleData[2];
         //auto resource = resourcePositions.find(refNodeName);
         //if (resource == resourcePositions.end())
         {
            instanceObjectNode->refNodeName = refNodeName;
            resourcePositions[refNodeName].emplace_back(
               Position
               { instanceObjectNode->posX, instanceObjectNode->posY, instanceObjectNode->posZ, 
                 instanceObjectNode->rotX, instanceObjectNode->rotY, instanceObjectNode->rotZ,
                 instanceObjectNode->scaleX, instanceObjectNode->scaleY, instanceObjectNode->scaleZ
               }
            );
         }

         auto resourceName = getResourceName(instanceObjectNode->refNodePtr);
         //if (resourceName != "")
         {
            instanceObjectNode->resourceName = resourceName;

            //auto group = resourceGroup.find(resourceName);
            //if (group == resourceGroup.end())
            {
               resourceGroup[resourceName].push_back(refNodeName);
            }
         }

			result = instanceObjectNode;
		}
      else if (parentNodeName == "BaseObjects/BG/ResourceReferenceObject")
      {

      }
      else if (parentNodeName == "MiscObjects/LaySettings/LaySettingsObject")
      {
         inputStream.Seek(nodeAbsPtr, Framework::STREAM_SEEK_SET);
         inputStream.Read(&layoutSettings, sizeof(LAY_SETTINGS_OBJECT));
      }
		else if(parentNodeName == "RefObjects/UnitTree/UnitTreeObject")
		{
			auto unitTreeObjectNode = std::make_shared<UNIT_TREE_OBJECT_NODE>();
			uint32 nodeData[0x14];

			//0x07 -> Position Array Ptr?
			//0x08 -> Scale Array Ptr?
			//0x0B -> String Ptr
			//0x0C -> Item Array Ptr
			//0x0D -> Item Count

			inputStream.Seek(nodeAbsPtr, Framework::STREAM_SEEK_SET);
			inputStream.Read(nodeData, sizeof(nodeData));

         inputStream.Seek(nodeData[0x02] + headerSize + 0x30, Framework::STREAM_SEEK_SET);
         unitTreeObjectNode->name = inputStream.ReadString();


			inputStream.Seek(nodeData[0x0C] + headerSize + 0x30, Framework::STREAM_SEEK_SET);
			unitTreeObjectNode->items.resize(nodeData[0x0D]);
         unitTreeObjectNode->nodeId = nodeData[0x01];

         auto prevPtr = inputStream.Tell();

			for(auto& item : unitTreeObjectNode->items)
			{
				uint32 itemValues[0x0C];
				inputStream.Read(itemValues, sizeof(itemValues));

				prevPtr = inputStream.Tell();

				inputStream.Seek(itemValues[0x05] + headerSize + 0x30, Framework::STREAM_SEEK_SET);
				item.name = inputStream.ReadString();

				inputStream.Seek(prevPtr, Framework::STREAM_SEEK_SET);

				item.nodePtr = itemValues[0x06];

            unitTreeItemCount++;

            auto resourceName = getResourceName(item.nodePtr);
            //if (resourceName != "")
            {
               item.resourceName = resourceName;
               //auto group = resourceGroup.find(resourceName);
               //if (group == resourceGroup.end())
               {
                  resourceGroup[resourceName].push_back(unitTreeObjectNode->name);
               }
            }
			}

			result = unitTreeObjectNode;
		}
		else if(parentNodeName == "BaseObjects/BG/BGPartsBaseObject" || parentNodeName == "BaseObjects/BG/BGChipBaseObject")
		{
			auto bgPartsBaseObjectNode = std::make_shared<BGPARTS_BASE_OBJECT_NODE>();

			uint8 nodeData[0x54];

			inputStream.Seek(nodeAbsPtr, Framework::STREAM_SEEK_SET);
			inputStream.Read(nodeData, sizeof(nodeData));

			uint32 someStringPtr1 = *reinterpret_cast<uint32*>(nodeData + 0x08);
			uint32 someStringPtr2 = *reinterpret_cast<uint32*>(nodeData + 0x20);

			uint32 someStringAbsPtr1 = someStringPtr1 + headerSize + 0x30;
			uint32 someStringAbsPtr2 = someStringPtr2 + headerSize + 0x30;

			float vec0x = *reinterpret_cast<float*>(nodeData + 0x3C);
			float vec0y = *reinterpret_cast<float*>(nodeData + 0x40);
			float vec0z = *reinterpret_cast<float*>(nodeData + 0x44);

			float vec1x = *reinterpret_cast<float*>(nodeData + 0x48);
			float vec1y = *reinterpret_cast<float*>(nodeData + 0x4C);
			float vec1z = *reinterpret_cast<float*>(nodeData + 0x50);

			inputStream.Seek(someStringAbsPtr1, Framework::STREAM_SEEK_SET);
			auto modelName = inputStream.ReadString();

			inputStream.Seek(someStringAbsPtr2, Framework::STREAM_SEEK_SET);
			auto resourceName = inputStream.ReadString();

			bgPartsBaseObjectNode->modelName = modelName;
			bgPartsBaseObjectNode->resourceName = resourceName;
			bgPartsBaseObjectNode->minX = vec0x;
			bgPartsBaseObjectNode->minY = vec0y;
			bgPartsBaseObjectNode->minZ = vec0z;
			bgPartsBaseObjectNode->maxX = vec1x;
			bgPartsBaseObjectNode->maxY = vec1y;
			bgPartsBaseObjectNode->maxZ = vec1z;

         filterModelName = modelName;
         type = parentNodeName == "BaseObjects/BG/BGPartsBaseObject" ? "parts" : "chip";
			result = bgPartsBaseObjectNode;
		}
		else
		{
			//result = std::make_shared<LAYOUT_NODE>();
		}

      if (result)
      {
         result->name = nodeName;
         result->parentPtr = parentPtr;
         result->nodeId = nodeId;
      }
      bool push = true;
      std::string reg = "";
      // lambda to parse filters
      auto filterFunc = [&] ( std::string filter, std::string mode ) -> bool
      {
         int offset = 0;
         auto inc = filter.substr( 0, filter.find_first_of( " ", 0 ) );
         std::string ogFilter = filter;
         bool isMatch = false;

         offset += inc.size();

         auto objType = filter.substr( offset + 1, filter.find_first_of( " ", offset ) - 1 );
         auto objMatch = objType.substr( 0, 2 ) == type.substr( 0, 2 ) || objType == "all";

         offset += objType.size();

         filter = filter.substr( offset + 2, filter.size() - 1 );
         if( filter.substr( 0, 2 ) == "re" )
         {
            std::regex re( reg = filter.substr( 3, filter.size() - 1 ) );

            if( std::regex_match( nodeName, re ) && objMatch )
            {
               if( inc.substr( 0, 3 ) == "exc" && mode == "exc" )
                  isMatch = !(push = false);
               else if( inc.substr( 0, 3 ) == "inc" && mode == "inc" )
               {
                  isMatch = (push = true);
                  fprintf( f, std::string( ogFilter + "|||" + filter + "|||" + nodeName + "\n" ).c_str() );
               }
            }
         }
         else
         {
            if( filterModelName == filter && objMatch )
            {
               if( inc.substr( 0, 3 ) == "exc" && mode == "exc" )
                  isMatch = !(push = false);
               else if( inc.substr( 0, 3 ) == "inc" && mode == "inc" )
                  isMatch = (push = true);
            }
         }
         return (isMatch && push && mode == "inc");
      };

	   for ( auto& filterStr : filters )
      {
         filterFunc( filterStr, "exc" );
      }
      for( auto& filterStr : filters )
      {
         filterFunc( filterStr, "inc" );
      }
      if (nodeName.find("isgrp") != -1 || filterModelName.find("isgrp") != -1)
      {
         fprintf(f, "\nFOUND GROUP\n%s\n%s\n", nodeName.c_str(), filterModelName.c_str());
         //throw std::runtime_error("isgrp found");
      }
      fclose( f );

      if( push && result )
		   m_layoutNodes.insert(std::make_pair(nodePtr, result));
	}
   if (parentNodeNames.size() > 1)
   {
      std::string placeholder;
   }
}