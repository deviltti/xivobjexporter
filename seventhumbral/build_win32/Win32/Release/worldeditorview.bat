SET map=%1
SET numThread=%2
SET hidden=%3
SET onemesh=%4
SET normals=%5

SET xivpath="C:/Program Files (x86)/SquareEnix/FINAL FANTASY XIV/"
WorldEditor path %xivpath% mapid %map% hidden %hidden% onemesh %onemesh% normals %normals%
cd ./data/obj/
objview %map%.obj %numThread%
cd ../../