#ifndef _EXPORTER_H
#define _EXPORTER_H

#include <vector>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cstdio>

#include <memory>
#include <set>

#include <filesystem>

#include "../../nlohmann/json.hpp"
#include "../../framework/include/math/Matrix4.h"
#include "../../framework/include/math/Vector2.h"
#include "../../framework/include/math/Vector3.h"
#include "../../framework/include/math/Vector4.h"

class Exporter;

class Exporter
{
public:
   struct Material;
   struct Mesh;
   struct Model;
   struct Transform;
   struct TransformedModel;
   struct Group;

   using MaterialPtr = std::shared_ptr<Material>;
   using MeshPtr = std::shared_ptr<Mesh>;
   using ModelPtr = std::shared_ptr<Model>;
   using TransformPtr = std::shared_ptr<Transform>;
   using TransformedModelPtr = std::shared_ptr<TransformedModel>;
   using GroupPtr = std::shared_ptr<Group>;

   struct Material : public std::enable_shared_from_this<Material>
   {
      uint32_t id;
      std::string name;
      std::string filePath;
      // <slot, filename.ext>
      std::map<std::string, std::string> textureMap;
   };

   struct Mesh : public std::enable_shared_from_this<Mesh>
   {
      uint32_t id;
      uint32_t materialRef;
      std::string name;
      std::vector<CVector3> vertices;
      std::vector<CVector3> uv1, uv2;
      std::vector<CVector4> colors;
      std::vector<CVector3> normals;
      std::vector<uint32_t> indices;

      bool Mesh::operator== (const Mesh& rhs) const
      {
         return id == rhs.id || name == rhs.name || (vertices.size() == rhs.vertices.size() && uv1.size() == rhs.uv1.size() &&
            colors.size() == rhs.uv1.size() && normals.size() == rhs.normals.size() && indices.size() == rhs.indices.size());
      }
   };

   struct Transform : public std::enable_shared_from_this<Transform>
   {
      CVector3 translation = CVector3(0, 0, 0);
      CVector4 rotation = CVector4(0, 0, 0, 1);
      CVector3 scale = CVector3(1, 1, 1);
      
      Transform(){}
      Transform(CVector3 translation, CVector4 rotation, CVector3 scale)
      {
         this->translation = translation;
         this->rotation = rotation;
         this->scale = scale;
      }
   };

   struct Model : public std::enable_shared_from_this<Model>
   {
      uint32_t id;
      std::string name;
      std::map<int, MeshPtr> meshes;
      Transform transformation;
   };

   struct TransformedModel : public std::enable_shared_from_this<TransformedModel>
   {
      uint32_t id;
      uint32_t modelRef;
      Transform transformation;
   };

   struct Group : public std::enable_shared_from_this<Group>
   {
      uint32_t id{ 0 };
      uint32_t type{ 1 };
      std::string name;
      Transform transformation;
      std::vector<TransformedModelPtr> models;
      std::vector<GroupPtr> subGroups;

      Group()
      {

      }
      void addGroup(GroupPtr pGroup)
      {
         //pGroup->parent = shared_from_this();
         subGroups.push_back(pGroup);
      }
   };
   
   std::map<std::string, uint32_t> materialNameRefMap;
   std::map<std::string, uint32_t> meshNameRefMap;
   std::map<std::string, uint32_t> modelNameRefMap;
   std::map<std::string, uint32_t> groupNameRefMap;

   std::map<uint32_t, MaterialPtr> materialIdMap;
   std::map<uint32_t, MeshPtr> meshIdMap;
   std::map<uint32_t, ModelPtr> modelIdMap;
   std::map<uint32_t, GroupPtr> groupIdMap;

   std::string exportDir = "./data/obj/";

   Exporter() { }
   ~Exporter(){}

   void reset()
   {
      this->materialIdMap.clear();
      this->materialNameRefMap.clear();
      this->modelIdMap.clear();
      this->modelNameRefMap.clear();
      this->groupIdMap.clear();
      this->groupNameRefMap.clear();
      this->meshIdMap.clear();
      this->meshNameRefMap.clear();
   }

   uint32_t addTransformedModelToGroup(TransformedModelPtr pModel, GroupPtr pGroup)
   {
      pGroup->models.push_back(pModel);
      pModel->id = pGroup->models.size();
      return pModel->id;
   }

   uint32_t addMeshToModel(MeshPtr pMesh, ModelPtr pModel)
   {
      auto it = meshNameRefMap.find(pMesh->name);

      if (it == meshNameRefMap.end())
      {
         pMesh->id = modelIdMap.size();
         meshNameRefMap.emplace(std::make_pair(pMesh->name, pMesh->id));
         meshIdMap[pMesh->id] = pMesh;
      }
      else
      {
         pMesh = meshIdMap[it->second];
      }
      //
      {
         for (auto& currMesh : pModel->meshes)
         {
            if (currMesh.second == pMesh)
            {
               return currMesh.first;
            }
         }
         pModel->meshes.emplace(pModel->meshes.size(), pMesh);
         //pMesh->modelIndex = pModel->meshes.size();
      }
      return pMesh->id;
   }

   MeshPtr getMesh(const std::string& name)
   {
      auto it = meshNameRefMap.find(name);

      if (it == meshNameRefMap.end())
      {
         return nullptr;
      }
      return meshIdMap[it->second];
   }

   // add model or return id
   uint32_t addModel(ModelPtr pModel)
   {
      auto it = modelNameRefMap.find(pModel->name);

      if (it == modelNameRefMap.end())
      {
         pModel->id = modelIdMap.size();
         modelNameRefMap.emplace(std::make_pair(pModel->name, pModel->id));
         modelIdMap[pModel->id] = pModel;
      }
      else
      {
         pModel = modelIdMap[it->second];
      }
      return pModel->id;
   }

   const ModelPtr getModel(const std::string& name) const
   {
      auto it = modelNameRefMap.find(name);
      if (it == modelNameRefMap.end())
         return nullptr;

      return modelIdMap.find(it->second)->second;
   }

   // add group or return id
   uint32_t addGroup(GroupPtr pGroup)
   {
      auto it = groupNameRefMap.find(pGroup->name);

      assert(it == groupNameRefMap.end());
      {
         pGroup->id = groupNameRefMap.size();
         groupNameRefMap.emplace(std::make_pair(pGroup->name, pGroup->id));
         groupIdMap[pGroup->id] = pGroup;
      }
      return pGroup->id;
   }

   GroupPtr getGroup(const std::string& name)
   {
      auto it = groupNameRefMap.find(name);
      if (it == groupNameRefMap.end())
         return nullptr;
      return groupIdMap[it->second];
   }

   // add material or get id
   uint32_t addMaterialToMesh(MaterialPtr pMaterial, MeshPtr pMesh)
   {
      auto it = materialNameRefMap.find(pMaterial->name);
      if (it == materialNameRefMap.end())
      {
         pMaterial->name = pMesh->name;
         pMaterial->id = materialNameRefMap.size();
         materialNameRefMap.emplace(std::make_pair(pMaterial->name, pMaterial->id));
         materialIdMap[pMaterial->id] = pMaterial;
      }
      else
      {
         pMaterial = materialIdMap[it->second];
      }
      pMesh->materialRef = pMaterial->id;
      return pMaterial->id;
   }

   MaterialPtr getMaterial(const std::string& name)
   {
      auto it = materialNameRefMap.find(name);
      if (it == materialNameRefMap.end())
         return nullptr;

      return materialIdMap[it->second];
   }

   void exportAllMaterials()
   {
      for (auto& matPair : materialIdMap)
      {
         auto pMaterial = matPair.second;
      }
   }

   void exportMaterialForMesh(MaterialPtr pMaterial, MeshPtr pMesh)
   {
      std::string mtlPath = exportDir + "textures/" + pMaterial->name + ".mtl";

      std::ofstream of(mtlPath, std::ios::trunc);
      of.close();
      of.open(mtlPath, std::ios::trunc);
      
      std::stringstream ss;

      ss << "newmtl " << pMaterial->name << "\n";
      for (const auto& texPair : pMaterial->textureMap)
      {
         ss << texPair.first << " " << texPair.second << "\n";
      }
      of << ss.str();
      of.close();
   }

   void exportObjForModel(ModelPtr pModel)
   {
      std::string objectDirPath = exportDir + "objects/";
      
      auto pos = pModel->transformation.translation;
      auto rot = pModel->transformation.rotation;
      auto scale = pModel->transformation.scale;

      for (auto& meshPair : pModel->meshes)
      {
         auto pMesh = meshPair.second;

         std::string meshName = pModel->name + "_" + std::to_string(meshPair.first);

         std::string meshPath = objectDirPath + meshName + ".obj";

         std::ofstream of(meshPath, std::ios::trunc);
         of.close();
         of.open(meshPath, std::ios::app);

         std::vector<std::string> fileLines;
         auto pMaterial = materialIdMap[pMesh->materialRef];

         exportMaterialForMesh(pMaterial, pMesh);


         fileLines.push_back("# exported for FFXIVHousingSim\n");
         fileLines.push_back("mtllib ..\\textures\\" + pMaterial->name + ".mtl" + "\n");
         fileLines.push_back("usemtl " + pMaterial->name + "\n");

         //fileLines.push_back("o " + meshName + "\n");
         uint32_t vCount = 0;
         uint32_t vtCount = 0;
         uint32_t vnCount = 0;


         fileLines.push_back("# vertices\n");
         for (auto v : pMesh->vertices)
         {

            auto xrot = CMatrix4::MakeAxisXRotation(rot.x);
            auto yrot = CMatrix4::MakeAxisYRotation(rot.y);
            auto zrot = CMatrix4::MakeAxisZRotation(rot.z);

            //v.x = (scale.x * v.x);
            //v.y = (scale.y * v.y);
            //v.z = (scale.z * v.z);

            //v = v * xrot;
            //v = v * yrot;
            //v = v * zrot;

            //v.x = v.x + pos.x;
            //v.y = v.y + pos.y;
            //v.z = v.z + pos.z;

            std::stringstream ss;
            ss << "v " << v.x << " " << v.y << " " << v.z << "\n";
            fileLines.push_back(ss.str());
         }

         fileLines.push_back("# vertex colors\n");
         for (auto vc : pMesh->colors)
         {
            std::stringstream ss;
            ss << "vc " << vc.x << " " << vc.y << " " << vc.z << " " << vc.w << "\n";
            fileLines.push_back(ss.str());
         }

         for (const auto& line : fileLines)
            of.write(line.c_str(), line.size());
         fileLines.clear();

         fileLines.push_back("# normals\n");
         for (auto n : pMesh->normals)
         {
            std::stringstream ss;
            ss << "vn " << n.x << " " << n.y << " " << n.z << "\n";
            fileLines.push_back(ss.str());
         }

         fileLines.push_back("# texcoords\n");
         for (auto i = 0; i < pMesh->uv1.size(); ++i)
         {
            auto uv1 = pMesh->uv1[i];
            auto uv2 = pMesh->uv2[i];

            std::stringstream ss;
            ss << "vt " << uv1.x << " " << uv1.y << " " << uv2.x << " " << uv2.y << "\n";
            fileLines.push_back(ss.str());
         }

         for (const auto& line : fileLines)
            of.write(line.c_str(), line.size());
         fileLines.clear();

         //fss << "g " << pMesh->name << "\n";

         fileLines.push_back("# faces\n");
         int polyCount = pMesh->indices.size() / 3;
         for (auto i = 0; i < polyCount; ++i)
         {
            fileLines.push_back("f ");
            for (auto j = 0; j < 3; ++j)
            {
               std::stringstream ss;
               auto idx = (i * 3);
               ss << (pMesh->indices[idx + j] + vCount + 1) << "/" << (pMesh->indices[idx + j] + vtCount + 1) << "/" << (pMesh->indices[idx + j] + vnCount + 1) << (j != 2 ? " " : "\n");
               fileLines.push_back(ss.str());
            }
         }
         for (const auto& line : fileLines)
            of.write(line.c_str(), line.size());
         fileLines.clear();

         //vCount += pMesh->vertices.size();
         //vnCount += pMesh->normals.size();
         //vtCount += pMesh->uv1.size();
         of.close();

      }
   }

   void exportModel(ModelPtr pModel, nlohmann::json& j)
   {
      // 
      //j[std::to_string(index)] = std::map<std::string, std::string>();
      std::map<std::string, std::string> strMap;
      nlohmann::json modelMap(strMap);
      auto& tform = pModel->transformation;
      auto& tl = tform.translation;
      auto& rot = tform.rotation;
      auto& scl = tform.scale;

      //groupMap.push_back(std::make_pair("id", pGroup->id));
      modelMap = { 
                     {"id", pModel->id}, 
                     {"modelPath", pModel->name + ".mdl"}, 
                     {"modelName", pModel->name}, 
                     {"numMeshes", pModel->meshes.size()} 
      };
      j = modelMap;
   }

   void exportTransformedModel(TransformedModelPtr pModel, nlohmann::json& j, int index = 0)
   {
      std::map<std::string, std::string> strMap;
      nlohmann::json modelMap(strMap);
      auto& tform = pModel->transformation;
      auto& tl = tform.translation;
      auto& rot = tform.rotation;
      auto& scl = tform.scale;

      modelMap = {
                     {"id", index}, {"modelId", pModel->modelRef},
                     {"transform",
                        {
                           {"translation", {{"x", tl.x}, {"y", tl.y}, {"z", tl.z}}},
                           {"rotation", {{"x", rot.x}, {"y", -1 * rot.y}, {"z", rot.z}, {"w", rot.w}}},
                           {"scale", {{"x", scl.x}, {"y", scl.y}, {"z", scl.z}}}
                        }
                     }
      };

      j.push_back(modelMap);
   }

   void exportGroup(GroupPtr pGroup, nlohmann::json& j, int index = -1)
   {
      // 
      //j[std::to_string(index)] = std::map<std::string, std::string>();
      std::map<std::string, std::string> strMap;
      nlohmann::json groupMap(strMap);
      auto& tform = pGroup->transformation;
      auto& tl = tform.translation;
      auto& rot = tform.rotation;
      auto& scl = tform.scale;

      groupMap["id"] = pGroup->id;
      groupMap["type"] = pGroup->type;
      groupMap["groupName"] = pGroup->name;
      groupMap["groupTransform"] =
      {
         {"translation", {{"x", tl.x}, {"y", tl.y}, {"z", tl.z}}},
         {"rotation", {{"x", rot.x}, {"y", rot.y}, {"z", rot.z}, {"w", rot.w}}},
         {"scale", {{"x", scl.x}, {"y", scl.y}, {"z", scl.z}}}
      };

      if (pGroup->subGroups.size() > 0)
         groupMap["groups"] = nlohmann::json::array();
      
      groupMap["entries"] = nlohmann::json::array();

      //auto subGroupMap = groupMap["groups"];

      int i = -1;
      for (const auto& childGroup : pGroup->subGroups)
      {
         exportGroup(childGroup, groupMap["groups"], i);
      }
      
      i = 0;
      for (const auto& transformedModel : pGroup->models)
      {
         exportTransformedModel(transformedModel, groupMap["entries"], i);
         ++i;
      }
      if (index != -1)
         j = groupMap;
      else
      {
         j.push_back(groupMap);
      }
   }

   void doExport(const std::string& fileName)
   {
      exportDir = "./data/obj/";

      using json = nlohmann::json;
      std::map<std::string, std::string> cmap;
      json j(cmap);
      j["groups"] = cmap;
      GroupPtr pGroup = std::make_shared<Group>();

      int index = 0;
      for (auto& group : groupIdMap)
      {
         j["groups"][std::to_string(group.first)] = cmap;
         exportGroup(group.second, j["groups"][std::to_string(group.first)], index);
         if (index == 0)
         {

         }
         index++;
      }
      //exportGroup(pGroup, j["groups"][std::to_string(0)]);

      j["models"] = {};
      for (auto& model : modelIdMap)
      {
         exportModel(model.second, j["models"][std::to_string(model.first)]);
         exportObjForModel(model.second);
      }
      auto str = j.dump(2);
      std::string jsonFileName = fileName;
      std::ofstream of(exportDir + jsonFileName, std::ios::trunc);
      of.close();
      of.open(exportDir + jsonFileName, std::ios::app);
      of << str;
      of.close();
   }
};

static Exporter g_Exporter;

#endif