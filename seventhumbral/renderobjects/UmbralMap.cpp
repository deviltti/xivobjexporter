#include "UmbralMap.h"
#include "ResourceManager.h"
#include "../dataobjects/FileManager.h"
#include <ctime>
#include <iostream>
#include <fstream>
#include <ostream>
#include <istream>

#include "../build_win32/Exporter.h"
//#include "ffxiv-collision-exporter/PhbFile.cpp"

CUmbralMap::CUmbralMap( const MapLayoutPtr& mapLayout, std::map<std::string, std::string> args )
{
   //g_Exporter.doExport("test");

   std::vector< uint32_t > phbIndices;
   uint32_t phbIdx = 0;
   std::map< std::string, int > resTypes;
   //std::map< uint32_t, PhbFile > phbFileIdMap;
   std::map< std::string, bool > exportedResources;
   bool hidden = args["hidden"] != "" && args["hidden"] != "0";


   std::string fileName;
   if (args["name"] != "")
   {
      if (args["name"].size() >= 6)
         fileName = (args["name"].find("dun") != -1 || args["name"].find("fld") != -1 ? "outdoors/" + args["name"].substr(0, 6) + "/" : "") + args["name"] + "_";
   }
   fileName = std::string("./data/obj/") + fileName + args["map"];;
   auto createObjFileForWrite = [&](const std::string& fName, int part = 0) -> FILE *
   {
      std::string name(fName + "_" + std::to_string(part) + ".obj");
      FILE* f = fopen(name.c_str(), "w+");
      if (!f) { throw std::runtime_error(std::string("Unable to create file " + name).c_str()); }
      fprintf(f, "");
      fclose(f);
      f = fopen(name.c_str(), "ab+");
      fprintf(f, ("o " + args["name"] + "_" + args["map"] + "_" + std::to_string(part) + "\n").c_str());

      CUmbralMesh::g_exportVertCount = 0;
      CUmbralMesh::g_exportVTCount = 0;
      CUmbralMesh::g_exportVNCount = 0;
      return f;
   };

   FILE* f = createObjFileForWrite(fileName.c_str());

   if (args["export"] == "" || args["export"] == "0")
   {
      f = nullptr;
   }

   int exportPart = 0;
   uint64_t exportMaxPoly = 0xFFFFFF;
   int unique = 0;
   int meshUnique = 0;

   bool oneMesh = args["onemesh"] != "" && args["onemesh"] != "0";
   CUmbralMesh::g_exportOverwriteTextures = args["overwrite"] != "" && args["overwrite"] != "0";
   if (args["maxpoly"] != "")
   {
      auto arg = args["maxpoly"];
      exportMaxPoly = std::stoull(arg);
      if (exportMaxPoly < 0xFF)
      {
         throw std::runtime_error("maxpoly is way too low, this will result in hundreds of obj files. usage: maxpoly [255+]");
      }
   }


   // load textures
   for( const auto& resourceItem : mapLayout->GetResourceItems() )
   {
      std::string resType(resourceItem.type);
      resTypes[resType]++;
      if( resType == "brt" || resType == "bxt" )
      {
         CResourceManager::GetInstance().LoadResource( resourceItem.resourceId, resourceItem.name );

      }
      else if (resType == "bhp")
      {
         //if (phbFileIdMap.find(resourceItem.resourceId) == phbFileIdMap.end())
         {
            //PhbFile phbFile;
            //auto filePath = CFileManager::GetResourcePath(resourceItem.resourceId).string();
            //if (phbFile.openImpl(filePath) == OpenResult::Success)
            //{
            //   phbFileIdMap.emplace( std::make_pair( resourceItem.resourceId, phbFile ));
            //}
         }
         phbIndices.push_back(phbIdx);
      }
      phbIdx++;
   }

   const auto& layoutNodes = mapLayout->GetLayoutNodes();

   //Build bg parts
   std::map< uint32_t, std::vector< UmbralModelPtr > > nodeModelPtrs;
   for( const auto& nodePair : layoutNodes )
   {
      if( auto bgPartObjectNode = std::dynamic_pointer_cast<CMapLayout::BGPARTS_BASE_OBJECT_NODE>( nodePair.second ) )
      {
         auto bgPartObject = CreateBgPartObject( mapLayout, bgPartObjectNode );
         assert( bgPartObject );
         m_bgPartObjects.insert( std::make_pair( nodePair.first, bgPartObject ) );

         auto pModel = std::make_shared<Exporter::Model>();
         pModel->name = bgPartObjectNode->modelName;
         pModel->transformation.translation = bgPartObject->GetPosition();
         pModel->transformation.scale = bgPartObject->GetScale();

         g_Exporter.addModel(pModel);

         auto tempNode = bgPartObject;

         tempNode->TraverseNodes([&](const Palleon::SceneNodePtr& node) {
            auto mesh = std::dynamic_pointer_cast<CUmbralMesh>(node);
            if (mesh)
            {

               Exporter::MeshPtr pMesh = g_Exporter.getMesh(pModel->name + "_" + std::to_string(meshUnique));
               if (!pMesh)
               {
                  pMesh = std::make_shared<Exporter::Mesh>();
                  pMesh->name = pModel->name + "_" + std::to_string(meshUnique);

                  mesh->RebuildIndices(f);

                  Exporter::MaterialPtr pMaterial = g_Exporter.getMaterial(pMesh->name);
                  if (pMaterial == nullptr)
                  {
                     pMaterial = std::make_shared<Exporter::Material>();
                     pMaterial->name = pMesh->name;
                  }
                  mesh->SetupTextures(f, pMaterial);

                  pMesh->indices = mesh->m_exportIndices;
                  pMesh->vertices = mesh->m_exportVecs;
                  pMesh->uv1 = mesh->m_exportUVs;
                  pMesh->uv2 = mesh->m_exportUV2s;
                  pMesh->normals = mesh->m_exportNorms;
                  pMesh->colors = mesh->m_exportCols;

                  g_Exporter.addMaterialToMesh(pMaterial, pMesh);
               }


               g_Exporter.addMeshToModel(pMesh, pModel);

               if (!hidden)
               {
                  auto instance = mesh->CreateInstance();
                  m_instances.push_back(instance);
               }

               meshUnique++;
            }
            return true;
            });

         meshUnique = 0;
         unique++;
      }
   }

   Exporter::GroupPtr pBgGroup = std::make_shared<Exporter::Group>();
   pBgGroup->name = args["name"] + "_" + args["map"];
   pBgGroup->type = 0;

   Exporter::GroupPtr pTerrainGroup = std::make_shared<Exporter::Group>();
   pTerrainGroup->name = "terrain";
   pTerrainGroup->type = 2; // terrain

   g_Exporter.addGroup(pBgGroup);
   g_Exporter.addGroup(pTerrainGroup);

   for (const auto& resource : mapLayout->m_resourceItems)
   {
      //auto resource = mapLayout->m_resourceItems[index];
      auto resIt = mapLayout->resourceGroup.find(resource.name);
      UmbralModelPtr bgPartObj = nullptr;

      std::string resourceName(&resource.name[0]);

      //auto phbFileIt = phbFileIdMap.find(resource.resourceId);
      //if (phbFileIt == phbFileIdMap.end())
      //   continue;

      if (exportedResources[resource.name])
         continue;
      exportedResources[resource.name] = true;
      //const auto phbVectors = phbFileIt->second.getVectors();
      
      std::string modelName;

      for (const auto& nodePair : mapLayout->m_layoutNodes)
      {
         auto bgPartObjNode = std::dynamic_pointer_cast< CMapLayout::BGPARTS_BASE_OBJECT_NODE >( nodePair.second );
         if (!bgPartObjNode)
            continue;

         if (bgPartObjNode->resourceName == resource.name)
         {
            modelName = bgPartObjNode->modelName;
            bgPartObj = m_bgPartObjects[nodePair.first];
            break;
         }
      }
      if (!bgPartObj)
         continue;

      Exporter::ModelPtr pModel = g_Exporter.getModel(modelName);
      if (pModel == nullptr)
         pModel = g_Exporter.getModel(resource.name);
      if (resIt != mapLayout->resourceGroup.end())
      {
         for (const auto& group : resIt->second)
         {
            Exporter::GroupPtr pGroup = g_Exporter.getGroup(group);
            if (pGroup == nullptr)
            {
               pGroup = std::make_shared<Exporter::Group>();
               pGroup->name = group;
               //pGroup->transformation = pModel->transformation;
            }
            auto resPosIt = mapLayout->resourcePositions.find(group);
            if (resPosIt != mapLayout->resourcePositions.end())
            {

               for (auto& pos : resPosIt->second)
               {
                  CVector3 position(pos.x, pos.y, pos.z);
                  //position += CVector3(mapLayout->layoutSettings.posX, mapLayout->layoutSettings.posY, mapLayout->layoutSettings.posZ);

                  CVector3 rotation(pos.rotX, pos.rotY, pos.rotZ);
                  CVector3 scale(pos.scaleX, pos.scaleY, pos.scaleZ);
                  CQuaternion rotY(CVector3(0, -1, 0), rotation.y);

                  Exporter::TransformedModelPtr pTransformedModel = std::make_shared<Exporter::TransformedModel>();
                  pTransformedModel->transformation.translation.x = position.x + pModel->transformation.translation.x;
                  pTransformedModel->transformation.translation.y = position.y + pModel->transformation.translation.y;
                  pTransformedModel->transformation.translation.z = position.z + pModel->transformation.translation.z;

                  pTransformedModel->transformation.rotation.x = rotY.x;
                  pTransformedModel->transformation.rotation.y = rotY.y;
                  pTransformedModel->transformation.rotation.z = rotY.z;
                  pTransformedModel->transformation.rotation.w = rotY.w;

                  pTransformedModel->transformation.scale.x = scale.x * pModel->transformation.scale.x;
                  pTransformedModel->transformation.scale.y = scale.y * pModel->transformation.scale.y;
                  pTransformedModel->transformation.scale.z = scale.z * pModel->transformation.scale.z;

                  pTransformedModel->modelRef = pModel->id;

                  if (pTransformedModel->modelRef == 0)
                  {
                     int x;
                  }

                  auto tempNode = Palleon::CSceneNode::Create();
                  tempNode->AppendChild(bgPartObj);
                  tempNode->SetPosition(position);
                  tempNode->SetRotation(rotY);
                  tempNode->SetScale(scale);
                  tempNode->UpdateTransformations();

                  tempNode->TraverseNodes([&](const Palleon::SceneNodePtr& node) {
                     auto mesh = std::dynamic_pointer_cast<CUmbralMesh>(node);
                     if (mesh)
                     {
                        if (!hidden)
                        {
                           auto instance = mesh->CreateInstance();
                           m_instances.push_back(instance);
                        }

                        meshUnique++;
                     }
                     return true;
                  });

                  g_Exporter.addTransformedModelToGroup(pTransformedModel, pGroup);

                  meshUnique = 0;
                  unique++;
               }
               pBgGroup->addGroup(pGroup);
            }
            else
            {
               //*
               CVector3 position(0, 0, 0);
               //position += CVector3(mapLayout->layoutSettings.posX, mapLayout->layoutSettings.posY, mapLayout->layoutSettings.posZ);
               CVector3 rot(0, 0, 0);
               CVector3 scale(1, 1, 1);
               CQuaternion rotY(CVector3(0, -1, 0), rot.y);

               Exporter::TransformedModelPtr pTransformedModel = std::make_shared<Exporter::TransformedModel>();
               pTransformedModel->transformation.translation.x = position.x + pModel->transformation.translation.x;
               pTransformedModel->transformation.translation.y = position.y + pModel->transformation.translation.y;
               pTransformedModel->transformation.translation.z = position.z + pModel->transformation.translation.z;

               pTransformedModel->transformation.rotation.x = rotY.x;
               pTransformedModel->transformation.rotation.y = rotY.y;
               pTransformedModel->transformation.rotation.z = rotY.z;
               pTransformedModel->transformation.rotation.w = rotY.w;

               pTransformedModel->transformation.scale.x = scale.x * pModel->transformation.scale.x;
               pTransformedModel->transformation.scale.y = scale.y * pModel->transformation.scale.y;
               pTransformedModel->transformation.scale.z = scale.z * pModel->transformation.scale.z;

               pTransformedModel->modelRef = pModel->id;

               if (pTransformedModel->modelRef == 0)
               {
                  int x;
               }

               auto tempNode = Palleon::CSceneNode::Create();
               tempNode->AppendChild(bgPartObj);
               tempNode->SetPosition(position);
               tempNode->SetRotation(rotY);
               tempNode->SetScale(scale);
               tempNode->UpdateTransformations();

               tempNode->TraverseNodes([&](const Palleon::SceneNodePtr& node) {
                  auto mesh = std::dynamic_pointer_cast<CUmbralMesh>(node);
                  if (mesh)
                  {
                     if (!hidden)
                     {
                        auto instance = mesh->CreateInstance();
                        m_instances.push_back(instance);
                     }

                     meshUnique++;
                  }
                  return true;
                  });

               g_Exporter.addTransformedModelToGroup(pTransformedModel, pGroup);

               meshUnique = 0;
               unique++;
            }
         }
      }
      else
      {
         std::string placeholder;
         //*
         CVector3 position(0, 0, 0);
         CVector3 rotation(0, 0, 0);
         CVector3 scale(1, 1, 1);
         CQuaternion rotY(CVector3(0, -1, 0), rotation.y);

         auto tempNode = Palleon::CSceneNode::Create();
         tempNode->AppendChild(bgPartObj);
         tempNode->SetPosition(position);
         tempNode->SetRotation(rotY);
         tempNode->SetScale(scale);
         tempNode->UpdateTransformations();

         Exporter::TransformedModelPtr pTransformedModel = std::make_shared<Exporter::TransformedModel>();
         pTransformedModel->transformation.translation.x = position.x + pModel->transformation.translation.x;
         pTransformedModel->transformation.translation.y = position.y + pModel->transformation.translation.y;
         pTransformedModel->transformation.translation.z = position.z + pModel->transformation.translation.z;

         pTransformedModel->transformation.rotation.x = rotY.x;
         pTransformedModel->transformation.rotation.y = rotY.y;
         pTransformedModel->transformation.rotation.z = rotY.z;
         pTransformedModel->transformation.rotation.w = rotY.w;

         pTransformedModel->transformation.scale.x = scale.x * pModel->transformation.scale.x;
         pTransformedModel->transformation.scale.y = scale.y * pModel->transformation.scale.y;
         pTransformedModel->transformation.scale.z = scale.z * pModel->transformation.scale.z;

         pTransformedModel->modelRef = pModel->id;

         if (pTransformedModel->modelRef == 0)
         {
            int x;
         }

         tempNode->TraverseNodes([&](const Palleon::SceneNodePtr& node) {
            auto mesh = std::dynamic_pointer_cast<CUmbralMesh>(node);
            if (mesh)
            {
               if (!hidden)
               {
                  auto instance = mesh->CreateInstance();
                  m_instances.push_back(instance);
               }

               meshUnique++;
            }
            return true;
            });

         g_Exporter.addTransformedModelToGroup(pTransformedModel, pTerrainGroup);

         meshUnique = 0;
         unique++;
      }
   }

   std::string outJson(args["name"] + "_" + args["map"] + ".json");

   g_Exporter.doExport(outJson);
   
   if (f)
   {
      fprintf(f, "# successfully exported %s", fileName.c_str());
      fclose(f);
   }
   std::ofstream of( "log.txt", std::ios_base::app );

   char bytes[] = { "01/01/1970 00:00:00" };
   auto timet = std::time( nullptr );
   auto tm = std::localtime( &timet );

   std::strftime( bytes, 20, "%d/%m/%Y %H:%M:%S", tm );
   of << "[" << &bytes[0] << "] " << "Done! " << fileName << std::endl;
   of << "Zone Position Offset: " << std::to_string(mapLayout->layoutSettings.posX) << " " <<
      std::to_string(mapLayout->layoutSettings.posY) << " " << std::to_string(mapLayout->layoutSettings.posZ) << std::endl;
   of.close();


   if( args["hidden"] != "" && args["hidden"] != "0" )
      exit( 0 );

}

CUmbralMap::~CUmbralMap()
{

}

void CUmbralMap::GetMeshes( Palleon::MeshArray& meshes, const Palleon::CCamera* camera )
{
   auto cameraFrustum = camera->GetFrustum();
   for( const auto& instance : m_instances )
   {
      auto boundingSphere = instance->GetWorldBoundingSphere();
      if( cameraFrustum.Intersects( boundingSphere ) )
      {
         meshes.push_back( instance.get() );
      }
   }
}

UmbralModelPtr CUmbralMap::CreateBgPartObject( const MapLayoutPtr& mapLayout, const std::shared_ptr<CMapLayout::BGPARTS_BASE_OBJECT_NODE>& node )
{
   UmbralModelPtr result;

   auto modelResource = CResourceManager::GetInstance().GetResource( node->resourceName );
   assert( modelResource );
   if( !modelResource ) 
      return result;

   auto modelChunk = modelResource->SelectNode<CModelChunk>();
   assert( modelChunk );
   if( !modelChunk ) 
      return result;

   CVector3 minPos( node->minX, node->minY, node->minZ );
   CVector3 maxPos( node->maxX, node->maxY, node->maxZ );

   CVector3 bgPartSize = ( maxPos - minPos ) / 2;
   CVector3 bgPartPos = ( maxPos + minPos ) / 2;

   result = std::make_shared<CUmbralModel>( modelChunk );
   result->SetPosition( bgPartPos );
   result->SetScale( bgPartSize );
   result->SetName(node->resourceName);

   result->TraverseNodes(
      [&] ( const Palleon::SceneNodePtr& node )
   {
      if( auto mesh = std::dynamic_pointer_cast<CUmbralMesh>( node ) )
      {
         mesh->RebuildIndices( );
      }
      return true;
   }
   );

   return result;
}
