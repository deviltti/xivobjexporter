#pragma once

#include "PalleonEngine.h"
#include "../dataobjects/ResourceDefs.h"
#include "D3DShader.h"
#include "../build_win32/Exporter.h"

class CUmbralMesh;
typedef std::shared_ptr<CUmbralMesh> UmbralMeshPtr;

struct Exporter::Material;
using MaterialPtr = std::shared_ptr<Exporter::Material>;

class CUmbralMesh : public Palleon::CMesh
{
public:
   CUmbralMesh();
   CUmbralMesh( const MeshChunkPtr&, const ShaderSectionPtr& );
   virtual								~CUmbralMesh();

   UmbralMeshPtr						CreateInstance() const;

   Palleon::EffectPtr					GetEffect() const;

   virtual void						Update( float ) override;

   void								SetLocalTexture( const ResourceNodePtr& );
   void								SetActivePolyGroups( uint32 );

   void								RebuildIndices( FILE* f = nullptr, CVector3* pVec = nullptr, CVector3* pSzVec = nullptr, CVector3* bgVec = nullptr, float rotX = 0, float rotY = 0, float rotZ = 0,
	   int unique = 0, int meshUnique = 0, const char* modelName = 0 );
   
   static std::map<std::string, bool> g_exportTexMap;
   static uint32_t g_exportVertCount, g_exportVTCount, g_exportVNCount;
   static bool g_exportOverwriteTextures;
   std::vector<CVector3> m_exportVecs, m_exportNorms, m_exportUVs, m_exportUV2s;
   std::vector<CVector4> m_exportCols;
   std::vector<uint32_t> m_exportIndices;
   std::map<std::string, std::vector<std::string>> m_exportTextureSlots;
   void								SetupTextures(FILE* fp = nullptr, MaterialPtr pMaterial = nullptr);

private:
   typedef std::map<std::string, unsigned int> SamplerRegisterMap;
   typedef CPgrpChunk::TriangleArray TriangleIndexArray;
   typedef std::map<unsigned int, TriangleIndexArray> PolyGroupMap;

   Palleon::VERTEX_BUFFER_DESCRIPTOR	GenerateVertexBufferDescriptor( const StreamChunkPtr&, const StreamChunkPtr& );

   void								SetupGeometry();
   void								SetupPolyGroups();
   void								SetupEffect();

   Palleon::EffectPtr					m_effect;

   MeshChunkPtr						m_meshChunk;
   ShaderSectionPtr					m_shaderSection;
   ResourceNodePtr						m_localTexture;
   SamplerRegisterMap					m_samplerRegisters;

   TriangleIndexArray					m_basePolyGroup;
   PolyGroupMap						m_polyGroups;
   uint32								m_activePolyGroups = 0;




   bool								m_indexRebuildNeeded = false;
};
